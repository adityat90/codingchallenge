# Coding Challenge
## To build and run the jar
`mvn package`  

`java -jar coding-challenge-1.0-SNAPSHOT-shaded.jar`

## Endpoints
### Accounts
POST /accounts - Creates an account. Response `Location` header contains the `accountId`   
Response Body  
`Empty`

| HttpStatus |         |
|------------|---------|
| 201        | Created |

---

GET /accounts/:accountId - Get account of `accountId` in current state  
Response Body  

```json
{
    "accountId": "String",
    "balance": Integer
}
```

| HttpStatus |                   |
|------------|-------------------|
| 200        | Ok                |
| 404        | Account not found |

---

POST /accounts/:accountId/credit - Credit account`accountId`  
Request Body
```json
{
	"amount": Integer
}
```
Response Body  
`Empty`

| HttpStatus |                   |
|------------|-------------------|
| 202        | Accepted          |
| 404        | Account not found |

---

POST /accounts/:accountId/debit - Debit account`accountId`  
Request Body
```json
{
	"amount": Integer
}
```
Response Body  
`Empty`

| HttpStatus |                    |
|------------|--------------------|
| 202        | Accepted           |
| 404        | Account not found  |
| 400        | Insufficient funds |

---

### Transactions
POST /transactions - Create a new transaction  
Request Body
```json
{
    "fromAccountId": "String",
    "toAccountId": "String",
    "amount": Integer
}
```
Response Body  
`Empty`

| HttpStatus |                    |
|------------|--------------------|
| 202        | Accepted           |
| 404        | Account not found  |

---

GET /transactions/:transactionId  
Request Body
`Empty`  

Response Body  
```json
{
    "transactionId": "String",
    "fromAccountId": "String",
    "toAccountId": "String",
    "transactionStatus": "PENDING" | "SUCCESS" | "FAILED"
}
```

| HttpStatus |                        |
|------------|------------------------|
| 200        | Ok                     |
| 404        | Transaction not found  |
