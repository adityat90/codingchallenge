package com.adityatalpade;

import com.adityatalpade.api.Server;
import com.adityatalpade.di.Module;
import com.google.inject.Guice;
import com.google.inject.Injector;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        Injector injector = Guice.createInjector(new Module());
        Server server = injector.getInstance(Server.class);
        server.start(3000);
    }
}
