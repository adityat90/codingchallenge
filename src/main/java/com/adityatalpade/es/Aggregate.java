package com.adityatalpade.es;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class Aggregate<ID extends AggregateId> {
    private ID aggregateId;
    private List<Event> changes = Collections.synchronizedList(new ArrayList<>());

    protected Aggregate(ID aggregateId) {
        this.aggregateId = aggregateId;
    }

    public ID getAggregateId() {
        return this.aggregateId;
    }

    public List<Event> getChanges() {
        return this.changes;
    }
}
