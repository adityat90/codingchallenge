package com.adityatalpade.es;

import java.util.List;

public interface EventStore {
    EventStream loadEventStream(AggregateId aggregateId);

    void store(AggregateId aggregateId, List<Event> changes);
}