package com.adityatalpade.es;

import java.util.List;

public interface EventStream {
    void addAll(List<Event> changes);

    List<Event> getStream();
}
