package com.adityatalpade.es;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class InMemoryEventStream implements EventStream {
    private final List<Event> stream;

    private InMemoryEventStream(List<Event> stream) {
        this.stream = stream;
    }

    public static InMemoryEventStream empty() {
        return new InMemoryEventStream(Collections.synchronizedList(new ArrayList<>()));
    }

    @Override
    public List<Event> getStream() {
        return this.stream;
    }

    @Override
    public void addAll(List<Event> changes) {
        stream.addAll(changes);
    }
}
