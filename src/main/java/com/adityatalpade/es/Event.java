package com.adityatalpade.es;

public interface Event<T extends Aggregate> {
    AggregateId getAggregateId();

    void applyOn(T aggregate);
}
