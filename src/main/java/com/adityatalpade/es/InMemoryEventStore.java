package com.adityatalpade.es;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InMemoryEventStore implements EventStore {
    private Map<AggregateId, EventStream> events = new HashMap<>();

    @Override
    public EventStream loadEventStream(AggregateId aggregateId) {
        return events.get(aggregateId);
    }

    @Override
    public void store(AggregateId aggregateId, List<Event> changes) {
        events.computeIfAbsent(aggregateId, id -> InMemoryEventStream.empty()).addAll(changes);
    }
}
