package com.adityatalpade.api;

import com.adityatalpade.api.controllers.AccountController;
import com.adityatalpade.api.controllers.TransactionController;
import com.google.inject.Inject;
import io.javalin.Javalin;

public class Server {
    public static final int ACCEPTED = 202;
    public static final int CREATED = 201;
    public static final String ACCOUNT_ENDPOINT = "/accounts";
    public static final String TRANSACTION_ENDPOINT = "/transactions";
    public static final String ID_PARAM = ":id";
    public static final String PATH_SEPARATOR = "/";
    public static final String CREDIT_ENDPOINT = "credit";
    public static final String DEBIT_ENDPOINT = "debit";

    private final AccountController accountController;
    private final TransactionController transactionController;

    @Inject
    public Server(AccountController accountController, TransactionController transactionController) {
        this.accountController = accountController;
        this.transactionController = transactionController;
    }

    public void start(Integer port) {
        Javalin.create()
                // Accounts
                .get(ACCOUNT_ENDPOINT + PATH_SEPARATOR + ID_PARAM, accountController::getAccount)
                .post(ACCOUNT_ENDPOINT, accountController::createAccount)
                .post(ACCOUNT_ENDPOINT + PATH_SEPARATOR + ID_PARAM + PATH_SEPARATOR + CREDIT_ENDPOINT, accountController::creditAccount)
                .post(ACCOUNT_ENDPOINT + PATH_SEPARATOR + ID_PARAM + PATH_SEPARATOR + DEBIT_ENDPOINT, accountController::debitAccount)
                // Transaction
                .get(TRANSACTION_ENDPOINT + PATH_SEPARATOR + ID_PARAM, transactionController::getTransaction)
                .post(TRANSACTION_ENDPOINT, transactionController::create)
                .start(port);
    }
}
