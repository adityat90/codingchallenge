package com.adityatalpade.api.controllers;

import com.adityatalpade.api.Server;
import com.adityatalpade.api.model.AccountProjection;
import com.adityatalpade.api.model.Credit;
import com.adityatalpade.api.model.Debit;
import com.adityatalpade.domain.Account;
import com.adityatalpade.domain.AccountId;
import com.adityatalpade.exception.AggregateNotFoundException;
import com.adityatalpade.exception.InsufficientFundsException;
import com.adityatalpade.exception.InvalidAmountException;
import com.adityatalpade.service.AccountService;
import com.google.inject.Inject;
import io.javalin.core.util.Header;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;

public class AccountController {
    private final AccountService accountService;

    @Inject
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    public void createAccount(Context context) {
        AccountId accountId = accountService.createAccount();
        context.header(Header.LOCATION, accountId.toString());
        context.status(Server.CREATED);
    }

    public void getAccount(Context context) {
        String accountId = context.pathParam(Server.ID_PARAM, String.class).get();
        Account account;
        try {
            account = accountService.loadAccount(AccountId.from(accountId));
        } catch (AggregateNotFoundException e) {
            throw new NotFoundResponse();
        }
        AccountProjection accountProjection = AccountProjection.fromAggregate(account);
        context.json(accountProjection);
    }

    public void creditAccount(Context context) {
        String accountId = context.pathParam(Server.ID_PARAM, String.class).get();
        Credit credit = context.bodyValidator(Credit.class).get();
        try {
            accountService.credit(AccountId.from(accountId), credit.getAmount());
        } catch (InvalidAmountException e) {
            throw new BadRequestResponse();
        } catch (AggregateNotFoundException e) {
            throw new NotFoundResponse();
        }
        context.status(Server.ACCEPTED);
    }

    public void debitAccount(Context context) {
        String accountId = context.pathParam(Server.ID_PARAM, String.class).get();
        Debit debit = context.bodyValidator(Debit.class).get();
        try {
            accountService.debit(AccountId.from(accountId), debit.getAmount());
        } catch (InvalidAmountException | InsufficientFundsException e) {
            throw new BadRequestResponse();
        } catch (AggregateNotFoundException e) {
            throw new NotFoundResponse();
        }
        context.status(Server.ACCEPTED);
    }
}
