package com.adityatalpade.api.controllers;

import com.adityatalpade.api.Server;
import com.adityatalpade.api.model.Transact;
import com.adityatalpade.api.model.TransactionProjection;
import com.adityatalpade.domain.AccountId;
import com.adityatalpade.domain.Transaction;
import com.adityatalpade.domain.TransactionId;
import com.adityatalpade.exception.AggregateNotFoundException;
import com.adityatalpade.exception.TransactionFailureException;
import com.adityatalpade.service.TransactionService;
import com.google.inject.Inject;
import io.javalin.core.util.Header;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;

public class TransactionController {
    private final TransactionService transactionService;

    @Inject
    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    public void getTransaction(Context context) {
        String transactionId = context.pathParam(Server.ID_PARAM, String.class).get();
        Transaction transaction;
        try {
            transaction = transactionService.loadTransaction(TransactionId.from(transactionId));
        } catch (AggregateNotFoundException e) {
            throw new NotFoundResponse();
        }
        TransactionProjection transactionProjection = TransactionProjection.fromAggregate(transaction);
        context.json(transactionProjection);
    }

    public void create(Context context) {
        Transact transact = context.bodyValidator(Transact.class).get();
        AccountId fromAccountId = AccountId.from(transact.getFromAccountId());
        AccountId toAccountId = AccountId.from(transact.getToAccountId());
        Integer amount = transact.getAmount();

        TransactionId transactionId = transactionService.createTransaction(fromAccountId, toAccountId, amount);
        try {
            transactionService.execute(transactionId);
        } catch (AggregateNotFoundException e) {
            throw new NotFoundResponse();
        } catch (TransactionFailureException e) {
            // Log and eat up  the exception
        }
        context.header(Header.LOCATION, transactionId.toString());
        context.status(Server.ACCEPTED);
    }
}
