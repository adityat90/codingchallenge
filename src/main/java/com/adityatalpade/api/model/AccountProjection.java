package com.adityatalpade.api.model;

import com.adityatalpade.domain.Account;
import com.adityatalpade.domain.AccountId;

public class AccountProjection {
    private String accountId;
    private Integer balance;

    private AccountProjection() {
    }

    private AccountProjection(AccountId accountId, Integer balance) {
        this.accountId = accountId.toString();
        this.balance = balance;
    }

    public static AccountProjection fromAggregate(Account account) {
        return new AccountProjection(account.getAggregateId(), account.getBalance());
    }

    public String getAccountId() {
        return accountId;
    }

    public Integer getBalance() {
        return balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AccountProjection)) return false;

        AccountProjection that = (AccountProjection) o;

        if (!getAccountId().equals(that.getAccountId())) return false;
        return getBalance().equals(that.getBalance());
    }

    @Override
    public int hashCode() {
        int result = getAccountId().hashCode();
        result = 31 * result + getBalance().hashCode();
        return result;
    }
}
