package com.adityatalpade.api.model;

public class Credit {

    private Integer amount;

    public Credit() {
        // Empty Constructor
    }

    public Credit(Integer amount) {
        this.amount = amount;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
