package com.adityatalpade.api.model;

public class Transact {
    private String fromAccountId;
    private String toAccountId;
    private Integer amount;

    public Transact() {
    }

    public Transact(String fromAccountId, String toAccountId, Integer amount) {
        this.fromAccountId = fromAccountId;
        this.toAccountId = toAccountId;
        this.amount = amount;
    }

    public String getFromAccountId() {
        return fromAccountId;
    }

    public String getToAccountId() {
        return toAccountId;
    }

    public Integer getAmount() {
        return amount;
    }
}
