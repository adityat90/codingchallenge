package com.adityatalpade.api.model;

public class Debit {

    private Integer amount;

    public Debit() {
        // Empty Constructor
    }

    public Debit(Integer amount) {
        this.amount = amount;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
