package com.adityatalpade.api.model;

import com.adityatalpade.domain.AccountId;
import com.adityatalpade.domain.Transaction;
import com.adityatalpade.domain.TransactionId;
import com.adityatalpade.domain.TransactionStatus;

public class TransactionProjection {
    private String transactionId;
    private String fromAccountId;
    private String toAccountId;
    private Integer amount;
    private String transactionStatus;

    public TransactionProjection(TransactionId aggregateId, AccountId fromAccount, AccountId toAccount, Integer amount, TransactionStatus transactionStatus) {
        this.transactionId = aggregateId.toString();
        this.fromAccountId = fromAccount.toString();
        this.toAccountId = toAccount.toString();
        this.amount = amount;
        this.transactionStatus = transactionStatus.toString();
    }

    public static TransactionProjection fromAggregate(Transaction transaction) {
        return new TransactionProjection(transaction.getAggregateId(), transaction.getFromAccount(), transaction.getToAccount(), transaction.getAmount(), transaction.getStatus());
    }

    public String getTransactionId() {
        return transactionId;
    }

    public String getFromAccountId() {
        return fromAccountId;
    }

    public String getToAccountId() {
        return toAccountId;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TransactionProjection)) return false;

        TransactionProjection that = (TransactionProjection) o;

        if (!getTransactionId().equals(that.getTransactionId())) return false;
        if (!getFromAccountId().equals(that.getFromAccountId())) return false;
        if (!getToAccountId().equals(that.getToAccountId())) return false;
        return getTransactionStatus().equals(that.getTransactionStatus());
    }

    @Override
    public int hashCode() {
        int result = getTransactionId().hashCode();
        result = 31 * result + getFromAccountId().hashCode();
        result = 31 * result + getToAccountId().hashCode();
        result = 31 * result + getTransactionStatus().hashCode();
        return result;
    }
}
