package com.adityatalpade.events;

import com.adityatalpade.domain.Account;
import com.adityatalpade.es.AggregateId;
import com.adityatalpade.es.Event;

public class AccountCreatedEvent implements Event<Account> {
    private final AggregateId aggregateId;

    public AccountCreatedEvent(AggregateId aggregateId) {
        this.aggregateId = aggregateId;
    }

    @Override
    public AggregateId getAggregateId() {
        return aggregateId;
    }

    @Override
    public void applyOn(Account aggregate) {
        aggregate.apply(this);
    }
}
