package com.adityatalpade.events;

import com.adityatalpade.domain.Transaction;
import com.adityatalpade.domain.TransactionId;
import com.adityatalpade.es.AggregateId;
import com.adityatalpade.es.Event;

public class TransactionSucceededEvent implements Event<Transaction> {
    private final TransactionId aggregateId;

    public TransactionSucceededEvent(TransactionId aggregateId) {
        this.aggregateId = aggregateId;
    }

    @Override
    public AggregateId getAggregateId() {
        return this.aggregateId;
    }

    @Override
    public void applyOn(Transaction aggregate) {
        aggregate.apply(this);
    }
}
