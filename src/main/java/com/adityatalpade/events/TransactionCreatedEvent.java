package com.adityatalpade.events;

import com.adityatalpade.domain.AccountId;
import com.adityatalpade.domain.Transaction;
import com.adityatalpade.domain.TransactionId;
import com.adityatalpade.es.AggregateId;
import com.adityatalpade.es.Event;

public class TransactionCreatedEvent implements Event<Transaction> {
    private final TransactionId aggregateId;
    private final AccountId fromAccount;
    private final AccountId toAccount;
    private final Integer amount;

    public TransactionCreatedEvent(TransactionId aggregateId, AccountId fromAccount, AccountId toAccount, Integer amount) {
        this.aggregateId = aggregateId;
        this.fromAccount = fromAccount;
        this.toAccount = toAccount;
        this.amount = amount;
    }

    public Integer getAmount() {
        return amount;
    }

    public AccountId getFromAccount() {
        return fromAccount;
    }

    public AccountId getToAccount() {
        return toAccount;
    }

    @Override
    public AggregateId getAggregateId() {
        return this.aggregateId;
    }

    @Override
    public void applyOn(Transaction aggregate) {
        aggregate.apply(this);
    }
}
