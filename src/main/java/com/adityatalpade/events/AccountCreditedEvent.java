package com.adityatalpade.events;

import com.adityatalpade.domain.Account;
import com.adityatalpade.es.AggregateId;
import com.adityatalpade.es.Event;

public class AccountCreditedEvent implements Event<Account> {
    private AggregateId aggregateId;

    private Integer amount;

    public AccountCreditedEvent(AggregateId aggregateId, Integer amount) {
        this.aggregateId = aggregateId;
        this.amount = amount;
    }

    public Integer getAmount() {
        return amount;
    }

    @Override
    public AggregateId getAggregateId() {
        return this.aggregateId;
    }

    @Override
    public void applyOn(Account aggregate) {
        aggregate.apply(this);
    }
}
