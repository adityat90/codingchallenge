package com.adityatalpade.di;

import com.adityatalpade.es.InMemoryEventStore;
import com.adityatalpade.repositories.AccountRepository;
import com.adityatalpade.repositories.TransactionRepository;
import com.google.inject.AbstractModule;

public class Module extends AbstractModule {
    @Override
    protected void configure() {
        AccountRepository accountRepository = new AccountRepository(new InMemoryEventStore());
        TransactionRepository transactionRepository = new TransactionRepository(new InMemoryEventStore());

        binder().bind(AccountRepository.class).toInstance(accountRepository);
        binder().bind(TransactionRepository.class).toInstance(transactionRepository);
    }
}
