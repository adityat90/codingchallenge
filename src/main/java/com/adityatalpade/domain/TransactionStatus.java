package com.adityatalpade.domain;

public enum TransactionStatus {
    PENDING,
    SUCCESS,
    FAILED
}
