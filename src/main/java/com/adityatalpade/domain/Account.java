package com.adityatalpade.domain;

import com.adityatalpade.es.Aggregate;
import com.adityatalpade.events.AccountCreatedEvent;
import com.adityatalpade.events.AccountCreditedEvent;
import com.adityatalpade.events.AccountDebitedEvent;
import com.adityatalpade.exception.InsufficientFundsException;
import com.adityatalpade.exception.InvalidAmountException;

public class Account extends Aggregate<AccountId> {
    private Integer balance;

    private Account(AccountId accountId) {
        super(accountId);
    }

    public static Account create() {
        Account account = new Account(AccountId.generate());
        account.balance = 0;
        account.getChanges().add(new AccountCreatedEvent(account.getAggregateId()));
        return account;
    }

    public static Account from(AccountId accountId) {
        return new Account(accountId);
    }

    public void apply(AccountCreatedEvent event) {
        this.balance = 0;
    }

    public void apply(AccountCreditedEvent event) {
        this.balance += event.getAmount();
    }

    public void apply(AccountDebitedEvent accountDebitedEvent) {
        this.balance -= accountDebitedEvent.getAmount();
    }

    public Account credit(Integer amount) throws InvalidAmountException {
        if (amount < 0) {
            throw new InvalidAmountException();
        }
        this.balance += amount;
        this.getChanges().add(new AccountCreditedEvent(this.getAggregateId(), amount));
        return this;
    }

    public Account debit(Integer amount) throws InvalidAmountException, InsufficientFundsException {
        if (amount < 0) {
            throw new InvalidAmountException();
        }
        if (this.balance - amount < 0) {
            throw new InsufficientFundsException();
        }
        this.balance -= amount;
        this.getChanges().add(new AccountDebitedEvent(this.getAggregateId(), amount));
        return this;
    }

    public Integer getBalance() {
        return balance;
    }
}
