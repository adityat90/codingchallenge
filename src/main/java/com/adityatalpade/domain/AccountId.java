package com.adityatalpade.domain;

import com.adityatalpade.es.AggregateId;

import java.util.UUID;

public class AccountId implements AggregateId {
    private final String value;

    private AccountId(String value) {
        this.value = value;
    }

    public static AccountId from(String value) {
        return new AccountId(value);
    }

    public static AccountId generate() {
        return new AccountId(UUID.randomUUID().toString());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AccountId)) return false;

        AccountId accountId = (AccountId) o;

        return value.equals(accountId.value);
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    @Override
    public String toString() {
        return value;
    }
}
