package com.adityatalpade.domain;

import com.adityatalpade.es.Aggregate;
import com.adityatalpade.events.TransactionCreatedEvent;
import com.adityatalpade.events.TransactionFailedEvent;
import com.adityatalpade.events.TransactionSucceededEvent;

public class Transaction extends Aggregate<TransactionId> {
    private TransactionStatus status;

    private AccountId fromAccount;

    private AccountId toAccount;

    private Integer amount;

    private Transaction(TransactionId transactionId) {
        super(transactionId);
    }

    public static Transaction create(AccountId fromAccount, AccountId toAccount, Integer amount) {
        Transaction transaction = new Transaction(TransactionId.generate());
        transaction.fromAccount = fromAccount;
        transaction.toAccount = toAccount;
        transaction.amount = amount;
        transaction.status = TransactionStatus.PENDING;
        transaction.getChanges().add(new TransactionCreatedEvent(transaction.getAggregateId(), fromAccount, toAccount, amount));
        return transaction;
    }

    public static Transaction from(TransactionId transactionId) {
        return new Transaction(transactionId);
    }

    public void disburse() {
        this.status = TransactionStatus.SUCCESS;
        this.getChanges().add(new TransactionSucceededEvent(this.getAggregateId()));
    }

    public void stop() {
        this.status = TransactionStatus.FAILED;
        this.getChanges().add(new TransactionFailedEvent(this.getAggregateId()));
    }

    public void apply(TransactionCreatedEvent event) {
        this.fromAccount = event.getFromAccount();
        this.toAccount = event.getToAccount();
        this.status = TransactionStatus.PENDING;
        this.amount = event.getAmount();
    }

    public void apply(TransactionSucceededEvent event) {
        this.status = TransactionStatus.SUCCESS;
    }

    public void apply(TransactionFailedEvent event) {
        this.status = TransactionStatus.FAILED;
    }

    public TransactionStatus getStatus() {
        return status;
    }

    public AccountId getFromAccount() {
        return fromAccount;
    }

    public AccountId getToAccount() {
        return toAccount;
    }

    public Integer getAmount() {
        return amount;
    }
}
