package com.adityatalpade.domain;

import com.adityatalpade.es.AggregateId;

import java.util.UUID;

public class TransactionId implements AggregateId {
    private final String value;

    private TransactionId(String value) {
        this.value = value;
    }

    public static TransactionId from(String value) {
        return new TransactionId(value);
    }

    public static TransactionId generate() {
        return new TransactionId(UUID.randomUUID().toString());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TransactionId)) return false;

        TransactionId accountId = (TransactionId) o;

        return value.equals(accountId.value);
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    @Override
    public String toString() {
        return value;
    }
}
