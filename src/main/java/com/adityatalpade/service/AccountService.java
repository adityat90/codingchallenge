package com.adityatalpade.service;

import com.adityatalpade.domain.Account;
import com.adityatalpade.domain.AccountId;
import com.adityatalpade.exception.AggregateNotFoundException;
import com.adityatalpade.exception.InsufficientFundsException;
import com.adityatalpade.exception.InvalidAmountException;
import com.adityatalpade.repositories.AccountRepository;
import com.google.inject.Inject;

public class AccountService {
    private AccountRepository repository;

    @Inject
    public AccountService(AccountRepository repository) {
        this.repository = repository;
    }

    synchronized public AccountId createAccount() {
        Account account = Account.create();
        repository.store(account);
        return account.getAggregateId();
    }

    synchronized public Account loadAccount(AccountId accountId) throws AggregateNotFoundException {
        return repository.load(accountId);
    }

    synchronized public void credit(AccountId accountId, Integer amount) throws InvalidAmountException, AggregateNotFoundException {
        Account account = repository.load(accountId);
        account.credit(amount);
        repository.store(account);
    }

    synchronized public void debit(AccountId accountId, Integer amount) throws InvalidAmountException, InsufficientFundsException, AggregateNotFoundException {
        Account account = repository.load(accountId);
        account.debit(amount);
        repository.store(account);
    }
}
