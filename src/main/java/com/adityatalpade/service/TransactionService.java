package com.adityatalpade.service;

import com.adityatalpade.domain.AccountId;
import com.adityatalpade.domain.Transaction;
import com.adityatalpade.domain.TransactionId;
import com.adityatalpade.exception.AggregateNotFoundException;
import com.adityatalpade.exception.InsufficientFundsException;
import com.adityatalpade.exception.InvalidAmountException;
import com.adityatalpade.exception.TransactionFailureException;
import com.adityatalpade.repositories.TransactionRepository;
import com.google.inject.Inject;

public class TransactionService {
    private TransactionRepository transactionRepository;
    private AccountService accountService;

    @Inject
    public TransactionService(TransactionRepository transactionRepository, AccountService accountService) {
        this.transactionRepository = transactionRepository;
        this.accountService = accountService;
    }

    synchronized public TransactionId createTransaction(AccountId fromAccount, AccountId toAccount, Integer amount) {
        Transaction transaction = Transaction.create(fromAccount, toAccount, amount);
        transactionRepository.store(transaction);
        return transaction.getAggregateId();
    }

    synchronized public Transaction loadTransaction(TransactionId transactionId) throws AggregateNotFoundException {
        return transactionRepository.load(transactionId);
    }

    synchronized public void execute(TransactionId transactionId) throws AggregateNotFoundException, TransactionFailureException {
        Transaction transaction = transactionRepository.load(transactionId);
        AccountId fromAccountId = transaction.getFromAccount();
        AccountId toAccountId = transaction.getToAccount();

        try {
            accountService.debit(fromAccountId, transaction.getAmount());
            accountService.credit(toAccountId, transaction.getAmount());
        } catch (InvalidAmountException | InsufficientFundsException e) {
            transaction.stop();
            transactionRepository.store(transaction);
            throw new TransactionFailureException();
        }
        transaction.disburse();
        transactionRepository.store(transaction);
    }
}
