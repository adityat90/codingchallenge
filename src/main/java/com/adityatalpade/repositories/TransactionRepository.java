package com.adityatalpade.repositories;

import com.adityatalpade.domain.Transaction;
import com.adityatalpade.domain.TransactionId;
import com.adityatalpade.es.Event;
import com.adityatalpade.es.EventStore;
import com.adityatalpade.es.EventStream;
import com.adityatalpade.exception.TransactionNotFoundException;
import com.google.inject.Inject;

import java.util.List;
import java.util.Optional;

public class TransactionRepository implements Repository<Transaction, TransactionId, TransactionNotFoundException> {
    private final EventStore eventStore;

    @Inject
    public TransactionRepository(EventStore eventStore) {
        this.eventStore = eventStore;
    }

    @Override
    public void store(Transaction aggregate) {
        eventStore.store(aggregate.getAggregateId(), aggregate.getChanges());
    }

    @Override
    public Transaction load(TransactionId aggregateId) throws TransactionNotFoundException {
        EventStream eventStream = Optional.ofNullable(eventStore.loadEventStream(aggregateId))
                .orElseThrow(TransactionNotFoundException::new);
        List<Event> events = eventStream.getStream();
        Transaction transaction = Transaction.from(aggregateId);
        synchronized (events) {
            for (Event next : events) {
                next.applyOn(transaction);
            }
        }
        return transaction;
    }
}
