package com.adityatalpade.repositories;

import com.adityatalpade.es.Aggregate;
import com.adityatalpade.es.AggregateId;
import com.adityatalpade.exception.AggregateNotFoundException;

public interface Repository<A extends Aggregate, ID extends AggregateId, E extends AggregateNotFoundException> {
    void store(A aggregate);

    A load(ID aggregateId) throws E;
}
