package com.adityatalpade.repositories;

import com.adityatalpade.domain.Account;
import com.adityatalpade.domain.AccountId;
import com.adityatalpade.es.Event;
import com.adityatalpade.es.EventStore;
import com.adityatalpade.es.EventStream;
import com.adityatalpade.exception.AccountNotFoundException;
import com.google.inject.Inject;

import java.util.List;
import java.util.Optional;

public class AccountRepository implements Repository<Account, AccountId, AccountNotFoundException> {
    private final EventStore eventStore;

    @Inject
    public AccountRepository(EventStore eventStore) {
        this.eventStore = eventStore;
    }

    @Override
    public void store(Account aggregate) {
        eventStore.store(aggregate.getAggregateId(), aggregate.getChanges());
    }

    @Override
    public Account load(AccountId aggregateId) throws AccountNotFoundException {
        EventStream eventStream = Optional.ofNullable(eventStore.loadEventStream(aggregateId))
                .orElseThrow(AccountNotFoundException::new);
        List<Event> events = eventStream.getStream();
        Account account = Account.from(aggregateId);
        synchronized (events) {
            for (Event next : events) {
                next.applyOn(account);
            }
        }
        return account;
    }
}
