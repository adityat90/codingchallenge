package com.adityatalpade.events;

import com.adityatalpade.domain.Transaction;
import com.adityatalpade.domain.TransactionId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class TransactionFailedEventTest {

    @Test
    void test_aggregateId() {
        TransactionId aggregateId = Mockito.mock(TransactionId.class);
        TransactionFailedEvent event = new TransactionFailedEvent(aggregateId);
        Assertions.assertEquals(aggregateId, event.getAggregateId());
    }

    @Test
    void test_applyOn() {
        Transaction aggregate = Mockito.mock(Transaction.class);
        TransactionId aggregateId = Mockito.mock(TransactionId.class);
        TransactionFailedEvent event = new TransactionFailedEvent(aggregateId);
        event.applyOn(aggregate);
        Mockito.verify(aggregate).apply(event);
    }
}