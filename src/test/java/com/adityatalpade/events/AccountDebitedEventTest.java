package com.adityatalpade.events;

import com.adityatalpade.domain.Account;
import com.adityatalpade.es.AggregateId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class AccountDebitedEventTest {

    @Test
    void test_aggregateId() {
        AggregateId aggregateId = Mockito.mock(AggregateId.class);
        AccountDebitedEvent event = new AccountDebitedEvent(aggregateId, 100);
        Assertions.assertEquals(aggregateId, event.getAggregateId());
        Assertions.assertEquals(100, event.getAmount());
    }

    @Test
    void test_applyOn() {
        Account aggregate = Mockito.mock(Account.class);
        AggregateId aggregateId = Mockito.mock(AggregateId.class);
        AccountDebitedEvent event = new AccountDebitedEvent(aggregateId, 100);
        event.applyOn(aggregate);
        Mockito.verify(aggregate).apply(event);
    }
}