package com.adityatalpade.events;

import com.adityatalpade.domain.AccountId;
import com.adityatalpade.domain.Transaction;
import com.adityatalpade.domain.TransactionId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class TransactionCreatedEventTest {

    @Test
    void test_aggregateId() {
        TransactionId aggregateId = Mockito.mock(TransactionId.class);
        AccountId fromAccountId = Mockito.mock(AccountId.class);
        AccountId toAccountId = Mockito.mock(AccountId.class);
        TransactionCreatedEvent event = new TransactionCreatedEvent(aggregateId, fromAccountId, toAccountId, 100);
        Assertions.assertEquals(aggregateId, event.getAggregateId());
        Assertions.assertEquals(100, event.getAmount());
        Assertions.assertEquals(fromAccountId, event.getFromAccount());
        Assertions.assertEquals(toAccountId, event.getToAccount());
    }

    @Test
    void test_applyOn() {
        Transaction aggregate = Mockito.mock(Transaction.class);
        TransactionId aggregateId = Mockito.mock(TransactionId.class);
        AccountId fromAccountId = Mockito.mock(AccountId.class);
        AccountId toAccountId = Mockito.mock(AccountId.class);
        TransactionCreatedEvent event = new TransactionCreatedEvent(aggregateId, fromAccountId, toAccountId, 100);
        event.applyOn(aggregate);
        Mockito.verify(aggregate).apply(event);
    }
}