package com.adityatalpade.events;

import com.adityatalpade.domain.Account;
import com.adityatalpade.es.AggregateId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class AccountCreatedEventTest {

    @Test
    void test_aggregateId() {
        AggregateId aggregateId = Mockito.mock(AggregateId.class);
        AccountCreatedEvent event = new AccountCreatedEvent(aggregateId);
        Assertions.assertEquals(aggregateId, event.getAggregateId());
    }

    @Test
    void test_applyOn() {
        Account aggregate = Mockito.mock(Account.class);
        AggregateId aggregateId = Mockito.mock(AggregateId.class);
        AccountCreatedEvent event = new AccountCreatedEvent(aggregateId);
        event.applyOn(aggregate);
        Mockito.verify(aggregate).apply(event);
    }
}