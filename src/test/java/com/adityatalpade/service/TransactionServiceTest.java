package com.adityatalpade.service;

import com.adityatalpade.domain.AccountId;
import com.adityatalpade.domain.Transaction;
import com.adityatalpade.domain.TransactionId;
import com.adityatalpade.exception.AggregateNotFoundException;
import com.adityatalpade.exception.InsufficientFundsException;
import com.adityatalpade.exception.InvalidAmountException;
import com.adityatalpade.exception.TransactionFailureException;
import com.adityatalpade.repositories.TransactionRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class TransactionServiceTest {
    @Test
    void test_createTransaction() {
        AccountId fromAccount = Mockito.mock(AccountId.class);
        AccountId toAccount = Mockito.mock(AccountId.class);
        TransactionRepository transactionRepository = Mockito.mock(TransactionRepository.class);
        AccountService accountService = Mockito.mock(AccountService.class);
        TransactionService transactionService = new TransactionService(transactionRepository, accountService);
        TransactionId transactionId = transactionService.createTransaction(fromAccount, toAccount, 100);

        Mockito.verify(transactionRepository).store(Mockito.any(Transaction.class));
        Assertions.assertNotNull(transactionId);
    }

    @Test
    void test_loadTransaction() throws AggregateNotFoundException {
        Transaction transaction = Mockito.mock(Transaction.class);

        TransactionId transactionId = Mockito.mock(TransactionId.class);
        AccountId fromAccount = Mockito.mock(AccountId.class);
        AccountId toAccount = Mockito.mock(AccountId.class);

        TransactionRepository transactionRepository = Mockito.mock(TransactionRepository.class);
        Mockito.when(transactionRepository.load(transactionId)).thenReturn(transaction);

        AccountService accountService = Mockito.mock(AccountService.class);
        TransactionService transactionService = new TransactionService(transactionRepository, accountService);
        transactionService.loadTransaction(transactionId);
        Mockito.verify(transactionRepository).load(transactionId);
    }

    @Test
    void test_execute() throws AggregateNotFoundException, InvalidAmountException, TransactionFailureException {
        Transaction transaction = Mockito.mock(Transaction.class);

        TransactionId transactionId = Mockito.mock(TransactionId.class);
        AccountId fromAccount = Mockito.mock(AccountId.class);
        AccountId toAccount = Mockito.mock(AccountId.class);

        Mockito.when(transaction.getAmount()).thenReturn(100);
        Mockito.when(transaction.getFromAccount()).thenReturn(fromAccount);
        Mockito.when(transaction.getToAccount()).thenReturn(toAccount);

        TransactionRepository transactionRepository = Mockito.mock(TransactionRepository.class);
        Mockito.when(transactionRepository.load(transactionId)).thenReturn(transaction);

        AccountService accountService = Mockito.mock(AccountService.class);
        TransactionService transactionService = new TransactionService(transactionRepository, accountService);
        transactionService.execute(transactionId);
        Mockito.verify(transaction).disburse();
        Mockito.verify(transactionRepository).store(transaction);
    }

    @Test
    void test_executeInsufficientFunds() throws AggregateNotFoundException, InvalidAmountException, TransactionFailureException, InsufficientFundsException {
        Transaction transaction = Mockito.mock(Transaction.class);

        TransactionId transactionId = Mockito.mock(TransactionId.class);
        AccountId fromAccount = Mockito.mock(AccountId.class);
        AccountId toAccount = Mockito.mock(AccountId.class);

        Mockito.when(transaction.getAmount()).thenReturn(100);
        Mockito.when(transaction.getFromAccount()).thenReturn(fromAccount);
        Mockito.when(transaction.getToAccount()).thenReturn(toAccount);

        TransactionRepository transactionRepository = Mockito.mock(TransactionRepository.class);
        Mockito.when(transactionRepository.load(transactionId)).thenReturn(transaction);

        AccountService accountService = Mockito.mock(AccountService.class);
        Mockito.doThrow(new InsufficientFundsException()).when(accountService).debit(fromAccount, 100);
        TransactionService transactionService = new TransactionService(transactionRepository, accountService);
        Assertions.assertThrows(TransactionFailureException.class, () -> {
            transactionService.execute(transactionId);
        });

        Mockito.verify(transaction).stop();
        Mockito.verify(transactionRepository).store(transaction);
    }

    @Test
    void test_executeInvalidAmount() throws AggregateNotFoundException, InvalidAmountException, TransactionFailureException, InsufficientFundsException {
        Transaction transaction = Mockito.mock(Transaction.class);

        TransactionId transactionId = Mockito.mock(TransactionId.class);
        AccountId fromAccount = Mockito.mock(AccountId.class);
        AccountId toAccount = Mockito.mock(AccountId.class);

        Mockito.when(transaction.getAmount()).thenReturn(-100);
        Mockito.when(transaction.getFromAccount()).thenReturn(fromAccount);
        Mockito.when(transaction.getToAccount()).thenReturn(toAccount);

        TransactionRepository transactionRepository = Mockito.mock(TransactionRepository.class);
        Mockito.when(transactionRepository.load(transactionId)).thenReturn(transaction);

        AccountService accountService = Mockito.mock(AccountService.class);
        Mockito.doThrow(new InvalidAmountException()).when(accountService).credit(toAccount, -100);
        TransactionService transactionService = new TransactionService(transactionRepository, accountService);
        Assertions.assertThrows(TransactionFailureException.class, () -> {
            transactionService.execute(transactionId);
        });

        Mockito.verify(transaction).stop();
        Mockito.verify(transactionRepository).store(transaction);
    }
}