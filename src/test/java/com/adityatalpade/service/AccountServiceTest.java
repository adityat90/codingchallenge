package com.adityatalpade.service;

import com.adityatalpade.domain.Account;
import com.adityatalpade.domain.AccountId;
import com.adityatalpade.exception.AggregateNotFoundException;
import com.adityatalpade.exception.InsufficientFundsException;
import com.adityatalpade.exception.InvalidAmountException;
import com.adityatalpade.repositories.AccountRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class AccountServiceTest {
    @Test
    void test_createAccount() {
        AccountRepository accountRepository = Mockito.mock(AccountRepository.class);
        AccountService accountService = new AccountService(accountRepository);
        AccountId accountId = accountService.createAccount();

        Mockito.verify(accountRepository).store(Mockito.any(Account.class));
        Assertions.assertNotNull(accountId);
    }

    @Test
    void test_loadAccount() throws AggregateNotFoundException {
        Account account = Mockito.mock(Account.class);
        AccountId accountId = Mockito.mock(AccountId.class);
        AccountRepository accountRepository = Mockito.mock(AccountRepository.class);
        Mockito.when(accountRepository.load(accountId)).thenReturn(account);
        AccountService accountService = new AccountService(accountRepository);
        Account returnedAccount = accountService.loadAccount(accountId);
        Mockito.verify(accountRepository).load(accountId);
    }

    @Test
    void test_creditAccount() throws AggregateNotFoundException, InvalidAmountException {
        Account account = Mockito.mock(Account.class);
        AccountId accountId = Mockito.mock(AccountId.class);
        AccountRepository accountRepository = Mockito.mock(AccountRepository.class);
        Mockito.when(accountRepository.load(accountId)).thenReturn(account);
        AccountService accountService = new AccountService(accountRepository);
        accountService.credit(accountId, 100);
        Mockito.verify(accountRepository).store(account);
        Mockito.verify(account).credit(100);
    }

    @Test
    void test_debitAccount() throws AggregateNotFoundException, InvalidAmountException, InsufficientFundsException {
        Account account = Mockito.mock(Account.class);
        AccountId accountId = Mockito.mock(AccountId.class);
        AccountRepository accountRepository = Mockito.mock(AccountRepository.class);
        Mockito.when(accountRepository.load(accountId)).thenReturn(account);
        AccountService accountService = new AccountService(accountRepository);
        accountService.debit(accountId, 100);
        Mockito.verify(accountRepository).store(account);
        Mockito.verify(account).debit(100);
    }
}