package com.adityatalpade.api.controllers;

import com.adityatalpade.api.Server;
import com.adityatalpade.api.model.AccountProjection;
import com.adityatalpade.api.model.Credit;
import com.adityatalpade.api.model.Debit;
import com.adityatalpade.domain.Account;
import com.adityatalpade.domain.AccountId;
import com.adityatalpade.exception.AggregateNotFoundException;
import com.adityatalpade.exception.InsufficientFundsException;
import com.adityatalpade.exception.InvalidAmountException;
import com.adityatalpade.service.AccountService;
import io.javalin.core.validation.Validator;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static io.javalin.core.util.Header.LOCATION;

class AccountControllerTest {

    private static final String ACCOUNT_ID = "ACCOUNT_ID";

    @Test
    void test_createAccount() {
        Context context = Mockito.mock(Context.class);
        AccountId accountId = Mockito.mock(AccountId.class);
        Mockito.when(accountId.toString()).thenReturn(ACCOUNT_ID);

        AccountService accountService = Mockito.mock(AccountService.class);
        Mockito.when(accountService.createAccount()).thenReturn(accountId);

        AccountController accountController = new AccountController(accountService);
        accountController.createAccount(context);

        Mockito.verify(accountService).createAccount();
        Mockito.verify(context).header(LOCATION, accountId.toString());
        Mockito.verify(context).status(Server.CREATED);
    }

    @Test
    void test_getAccount() throws AggregateNotFoundException {
        Validator validator = Mockito.mock(Validator.class);
        Mockito.when(validator.get()).thenReturn(ACCOUNT_ID);
        Context context = Mockito.mock(Context.class);
        Mockito.when(context.pathParam(Mockito.anyString(), Mockito.any())).thenReturn(validator);

        AccountId accountId = Mockito.mock(AccountId.class);
        Mockito.when(accountId.toString()).thenReturn(ACCOUNT_ID);

        Account account = Mockito.mock(Account.class);
        Mockito.when(account.getAggregateId()).thenReturn(accountId);
        Mockito.when(account.getBalance()).thenReturn(100);

        AccountService accountService = Mockito.mock(AccountService.class);
        Mockito.when(accountService.loadAccount(Mockito.any(AccountId.class))).thenReturn(account);

        AccountProjection accountProjection = AccountProjection.fromAggregate(account);

        AccountController accountController = new AccountController(accountService);
        accountController.getAccount(context);

        Mockito.verify(context).json(accountProjection);
    }

    @Test
    void test_getAccountNotFound() throws AggregateNotFoundException {
        Validator validator = Mockito.mock(Validator.class);
        Mockito.when(validator.get()).thenReturn(ACCOUNT_ID);
        Context context = Mockito.mock(Context.class);
        Mockito.when(context.pathParam(Mockito.anyString(), Mockito.any())).thenReturn(validator);

        AccountId accountId = Mockito.mock(AccountId.class);
        Mockito.when(accountId.toString()).thenReturn(ACCOUNT_ID);

        Account account = Mockito.mock(Account.class);
        Mockito.when(account.getAggregateId()).thenReturn(accountId);
        Mockito.when(account.getBalance()).thenReturn(100);

        AccountService accountService = Mockito.mock(AccountService.class);
        Mockito.when(accountService.loadAccount(Mockito.any(AccountId.class))).thenThrow(new AggregateNotFoundException());

        AccountProjection accountProjection = AccountProjection.fromAggregate(account);

        AccountController accountController = new AccountController(accountService);
        Assertions.assertThrows(NotFoundResponse.class, () -> {
            accountController.getAccount(context);
        });
    }

    @Test
    void test_creditAccount() throws AggregateNotFoundException {
        Credit credit = Mockito.mock(Credit.class);
        Mockito.when(credit.getAmount()).thenReturn(100);

        Validator paramValidator = Mockito.mock(Validator.class);
        Mockito.when(paramValidator.get()).thenReturn(ACCOUNT_ID);

        Validator bodyValidator = Mockito.mock(Validator.class);
        Mockito.when(bodyValidator.get()).thenReturn(credit);

        Context context = Mockito.mock(Context.class);
        Mockito.when(context.pathParam(Mockito.anyString(), Mockito.any())).thenReturn(paramValidator);
        Mockito.when(context.bodyValidator(Mockito.any())).thenReturn(bodyValidator);

        AccountId accountId = Mockito.mock(AccountId.class);
        Mockito.when(accountId.toString()).thenReturn(ACCOUNT_ID);

        Account account = Mockito.mock(Account.class);
        Mockito.when(account.getAggregateId()).thenReturn(accountId);
        Mockito.when(account.getBalance()).thenReturn(100);

        AccountService accountService = Mockito.mock(AccountService.class);

        AccountController accountController = new AccountController(accountService);
        accountController.creditAccount(context);

        Mockito.verify(context).status(Server.ACCEPTED);
    }

    @Test
    void test_creditAccountInvalidAmount() throws AggregateNotFoundException, InvalidAmountException {
        Credit credit = Mockito.mock(Credit.class);
        Mockito.when(credit.getAmount()).thenReturn(100);

        Validator paramValidator = Mockito.mock(Validator.class);
        Mockito.when(paramValidator.get()).thenReturn(ACCOUNT_ID);

        Validator bodyValidator = Mockito.mock(Validator.class);
        Mockito.when(bodyValidator.get()).thenReturn(credit);

        Context context = Mockito.mock(Context.class);
        Mockito.when(context.pathParam(Mockito.anyString(), Mockito.any())).thenReturn(paramValidator);
        Mockito.when(context.bodyValidator(Mockito.any())).thenReturn(bodyValidator);

        AccountId accountId = Mockito.mock(AccountId.class);
        Mockito.when(accountId.toString()).thenReturn(ACCOUNT_ID);

        Account account = Mockito.mock(Account.class);
        Mockito.when(account.getAggregateId()).thenReturn(accountId);
        Mockito.when(account.getBalance()).thenReturn(100);

        AccountService accountService = Mockito.mock(AccountService.class);
        Mockito.doThrow(new InvalidAmountException()).when(accountService).credit(Mockito.any(), Mockito.any());

        AccountController accountController = new AccountController(accountService);

        Assertions.assertThrows(BadRequestResponse.class, () -> {
            accountController.creditAccount(context);
        });
    }

    @Test
    void test_creditAccountInvalidAccount() throws AggregateNotFoundException, InvalidAmountException {
        Credit credit = Mockito.mock(Credit.class);
        Mockito.when(credit.getAmount()).thenReturn(100);

        Validator paramValidator = Mockito.mock(Validator.class);
        Mockito.when(paramValidator.get()).thenReturn(ACCOUNT_ID);

        Validator bodyValidator = Mockito.mock(Validator.class);
        Mockito.when(bodyValidator.get()).thenReturn(credit);

        Context context = Mockito.mock(Context.class);
        Mockito.when(context.pathParam(Mockito.anyString(), Mockito.any())).thenReturn(paramValidator);
        Mockito.when(context.bodyValidator(Mockito.any())).thenReturn(bodyValidator);

        AccountId accountId = Mockito.mock(AccountId.class);
        Mockito.when(accountId.toString()).thenReturn(ACCOUNT_ID);

        Account account = Mockito.mock(Account.class);
        Mockito.when(account.getAggregateId()).thenReturn(accountId);
        Mockito.when(account.getBalance()).thenReturn(100);

        AccountService accountService = Mockito.mock(AccountService.class);
        Mockito.doThrow(new AggregateNotFoundException()).when(accountService).credit(Mockito.any(), Mockito.any());

        AccountController accountController = new AccountController(accountService);

        Assertions.assertThrows(NotFoundResponse.class, () -> {
            accountController.creditAccount(context);
        });
    }

    @Test
    void test_debitAccount() throws AggregateNotFoundException {
        Debit debit = Mockito.mock(Debit.class);
        Mockito.when(debit.getAmount()).thenReturn(100);

        Validator paramValidator = Mockito.mock(Validator.class);
        Mockito.when(paramValidator.get()).thenReturn(ACCOUNT_ID);

        Validator bodyValidator = Mockito.mock(Validator.class);
        Mockito.when(bodyValidator.get()).thenReturn(debit);

        Context context = Mockito.mock(Context.class);
        Mockito.when(context.pathParam(Mockito.anyString(), Mockito.any())).thenReturn(paramValidator);
        Mockito.when(context.bodyValidator(Mockito.any())).thenReturn(bodyValidator);

        AccountId accountId = Mockito.mock(AccountId.class);
        Mockito.when(accountId.toString()).thenReturn(ACCOUNT_ID);

        Account account = Mockito.mock(Account.class);
        Mockito.when(account.getAggregateId()).thenReturn(accountId);
        Mockito.when(account.getBalance()).thenReturn(100);

        AccountService accountService = Mockito.mock(AccountService.class);

        AccountController accountController = new AccountController(accountService);
        accountController.debitAccount(context);

        Mockito.verify(context).status(Server.ACCEPTED);
    }

    @Test
    void test_debitAccountInvalidAmount() throws AggregateNotFoundException, InvalidAmountException, InsufficientFundsException {
        Debit debit = Mockito.mock(Debit.class);
        Mockito.when(debit.getAmount()).thenReturn(100);

        Validator paramValidator = Mockito.mock(Validator.class);
        Mockito.when(paramValidator.get()).thenReturn(ACCOUNT_ID);

        Validator bodyValidator = Mockito.mock(Validator.class);
        Mockito.when(bodyValidator.get()).thenReturn(debit);

        Context context = Mockito.mock(Context.class);
        Mockito.when(context.pathParam(Mockito.anyString(), Mockito.any())).thenReturn(paramValidator);
        Mockito.when(context.bodyValidator(Mockito.any())).thenReturn(bodyValidator);

        AccountId accountId = Mockito.mock(AccountId.class);
        Mockito.when(accountId.toString()).thenReturn(ACCOUNT_ID);

        Account account = Mockito.mock(Account.class);
        Mockito.when(account.getAggregateId()).thenReturn(accountId);
        Mockito.when(account.getBalance()).thenReturn(100);

        AccountService accountService = Mockito.mock(AccountService.class);
        Mockito.doThrow(new InvalidAmountException()).when(accountService).debit(Mockito.any(), Mockito.any());

        AccountController accountController = new AccountController(accountService);

        Assertions.assertThrows(BadRequestResponse.class, () -> {
            accountController.debitAccount(context);
        });
    }

    @Test
    void test_debitAccountInsufficientFunds() throws AggregateNotFoundException, InvalidAmountException, InsufficientFundsException {
        Debit debit = Mockito.mock(Debit.class);
        Mockito.when(debit.getAmount()).thenReturn(100);

        Validator paramValidator = Mockito.mock(Validator.class);
        Mockito.when(paramValidator.get()).thenReturn(ACCOUNT_ID);

        Validator bodyValidator = Mockito.mock(Validator.class);
        Mockito.when(bodyValidator.get()).thenReturn(debit);

        Context context = Mockito.mock(Context.class);
        Mockito.when(context.pathParam(Mockito.anyString(), Mockito.any())).thenReturn(paramValidator);
        Mockito.when(context.bodyValidator(Mockito.any())).thenReturn(bodyValidator);

        AccountId accountId = Mockito.mock(AccountId.class);
        Mockito.when(accountId.toString()).thenReturn(ACCOUNT_ID);

        Account account = Mockito.mock(Account.class);
        Mockito.when(account.getAggregateId()).thenReturn(accountId);
        Mockito.when(account.getBalance()).thenReturn(100);

        AccountService accountService = Mockito.mock(AccountService.class);
        Mockito.doThrow(new InsufficientFundsException()).when(accountService).debit(Mockito.any(), Mockito.any());

        AccountController accountController = new AccountController(accountService);

        Assertions.assertThrows(BadRequestResponse.class, () -> {
            accountController.debitAccount(context);
        });
    }

    @Test
    void test_debitAccountInvalidAccount() throws AggregateNotFoundException, InvalidAmountException, InsufficientFundsException {
        Debit debit = Mockito.mock(Debit.class);
        Mockito.when(debit.getAmount()).thenReturn(100);

        Validator paramValidator = Mockito.mock(Validator.class);
        Mockito.when(paramValidator.get()).thenReturn(ACCOUNT_ID);

        Validator bodyValidator = Mockito.mock(Validator.class);
        Mockito.when(bodyValidator.get()).thenReturn(debit);

        Context context = Mockito.mock(Context.class);
        Mockito.when(context.pathParam(Mockito.anyString(), Mockito.any())).thenReturn(paramValidator);
        Mockito.when(context.bodyValidator(Mockito.any())).thenReturn(bodyValidator);

        AccountId accountId = Mockito.mock(AccountId.class);
        Mockito.when(accountId.toString()).thenReturn(ACCOUNT_ID);

        Account account = Mockito.mock(Account.class);
        Mockito.when(account.getAggregateId()).thenReturn(accountId);
        Mockito.when(account.getBalance()).thenReturn(100);

        AccountService accountService = Mockito.mock(AccountService.class);
        Mockito.doThrow(new AggregateNotFoundException()).when(accountService).debit(Mockito.any(), Mockito.any());

        AccountController accountController = new AccountController(accountService);

        Assertions.assertThrows(NotFoundResponse.class, () -> {
            accountController.debitAccount(context);
        });
    }
}