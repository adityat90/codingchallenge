package com.adityatalpade.api.controllers;

import com.adityatalpade.api.Server;
import com.adityatalpade.api.model.Transact;
import com.adityatalpade.api.model.TransactionProjection;
import com.adityatalpade.domain.AccountId;
import com.adityatalpade.domain.Transaction;
import com.adityatalpade.domain.TransactionId;
import com.adityatalpade.domain.TransactionStatus;
import com.adityatalpade.exception.AggregateNotFoundException;
import com.adityatalpade.exception.TransactionFailureException;
import com.adityatalpade.service.TransactionService;
import io.javalin.core.util.Header;
import io.javalin.core.validation.Validator;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class TransactionControllerTest {
    private static final String TRANSACTION_ID = "TRANSACTION_ID";

    @Test
    void test_getTransaction() throws AggregateNotFoundException {
        Validator validator = Mockito.mock(Validator.class);
        Mockito.when(validator.get()).thenReturn(TRANSACTION_ID);
        Context context = Mockito.mock(Context.class);
        Mockito.when(context.pathParam(Mockito.anyString(), Mockito.any())).thenReturn(validator);

        AccountId fromAccountId = Mockito.mock(AccountId.class);
        AccountId toAccountId = Mockito.mock(AccountId.class);

        TransactionId transactionId = Mockito.mock(TransactionId.class);
        Mockito.when(transactionId.toString()).thenReturn(TRANSACTION_ID);

        Transaction transaction = Mockito.mock(Transaction.class);
        Mockito.when(transaction.getAggregateId()).thenReturn(transactionId);
        Mockito.when(transaction.getAmount()).thenReturn(100);
        Mockito.when(transaction.getFromAccount()).thenReturn(fromAccountId);
        Mockito.when(transaction.getToAccount()).thenReturn(toAccountId);
        Mockito.when(transaction.getStatus()).thenReturn(TransactionStatus.PENDING);

        TransactionService transactionService = Mockito.mock(TransactionService.class);
        Mockito.when(transactionService.loadTransaction(Mockito.any(TransactionId.class))).thenReturn(transaction);

        TransactionProjection transactionProjection = TransactionProjection.fromAggregate(transaction);

        TransactionController transactionController = new TransactionController(transactionService);
        transactionController.getTransaction(context);

        Mockito.verify(context).json(transactionProjection);
    }

    @Test
    void test_getTransactionNotFound() throws AggregateNotFoundException {
        Validator validator = Mockito.mock(Validator.class);
        Mockito.when(validator.get()).thenReturn(TRANSACTION_ID);
        Context context = Mockito.mock(Context.class);
        Mockito.when(context.pathParam(Mockito.anyString(), Mockito.any())).thenReturn(validator);

        AccountId fromAccountId = Mockito.mock(AccountId.class);
        AccountId toAccountId = Mockito.mock(AccountId.class);

        TransactionId transactionId = Mockito.mock(TransactionId.class);
        Mockito.when(transactionId.toString()).thenReturn(TRANSACTION_ID);

        Transaction transaction = Mockito.mock(Transaction.class);
        Mockito.when(transaction.getAggregateId()).thenReturn(transactionId);
        Mockito.when(transaction.getAmount()).thenReturn(100);
        Mockito.when(transaction.getFromAccount()).thenReturn(fromAccountId);
        Mockito.when(transaction.getToAccount()).thenReturn(toAccountId);
        Mockito.when(transaction.getStatus()).thenReturn(TransactionStatus.PENDING);

        TransactionService transactionService = Mockito.mock(TransactionService.class);
        Mockito.when(transactionService.loadTransaction(Mockito.any(TransactionId.class))).thenThrow(new AggregateNotFoundException());

        TransactionProjection transactionProjection = TransactionProjection.fromAggregate(transaction);

        TransactionController transactionController = new TransactionController(transactionService);
        Assertions.assertThrows(NotFoundResponse.class, () -> {
            transactionController.getTransaction(context);
        });
    }

    @Test
    void test_create() throws AggregateNotFoundException {
        Transact transact = Mockito.mock(Transact.class);
        Validator bodyValidator = Mockito.mock(Validator.class);
        Mockito.when(bodyValidator.get()).thenReturn(transact);

        Validator paramValidator = Mockito.mock(Validator.class);
        Mockito.when(paramValidator.get()).thenReturn(TRANSACTION_ID);
        Context context = Mockito.mock(Context.class);
        Mockito.when(context.pathParam(Mockito.anyString(), Mockito.any())).thenReturn(paramValidator);
        Mockito.when(context.bodyValidator(Mockito.any())).thenReturn(bodyValidator);

        AccountId fromAccountId = Mockito.mock(AccountId.class);
        AccountId toAccountId = Mockito.mock(AccountId.class);

        TransactionId transactionId = Mockito.mock(TransactionId.class);
        Mockito.when(transactionId.toString()).thenReturn(TRANSACTION_ID);

        Transaction transaction = Mockito.mock(Transaction.class);
        Mockito.when(transaction.getAggregateId()).thenReturn(transactionId);
        Mockito.when(transaction.getAmount()).thenReturn(100);
        Mockito.when(transaction.getFromAccount()).thenReturn(fromAccountId);
        Mockito.when(transaction.getToAccount()).thenReturn(toAccountId);
        Mockito.when(transaction.getStatus()).thenReturn(TransactionStatus.PENDING);

        TransactionService transactionService = Mockito.mock(TransactionService.class);
        Mockito.when(transactionService.createTransaction(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(transactionId);

        TransactionController transactionController = new TransactionController(transactionService);
        transactionController.create(context);

        Mockito.verify(context).header(Header.LOCATION, transactionId.toString());
        Mockito.verify(context).status(Server.ACCEPTED);
    }

    @Test
    void test_createNotFoundException() throws AggregateNotFoundException, TransactionFailureException {
        Transact transact = Mockito.mock(Transact.class);
        Validator bodyValidator = Mockito.mock(Validator.class);
        Mockito.when(bodyValidator.get()).thenReturn(transact);

        Validator paramValidator = Mockito.mock(Validator.class);
        Mockito.when(paramValidator.get()).thenReturn(TRANSACTION_ID);
        Context context = Mockito.mock(Context.class);
        Mockito.when(context.pathParam(Mockito.anyString(), Mockito.any())).thenReturn(paramValidator);
        Mockito.when(context.bodyValidator(Mockito.any())).thenReturn(bodyValidator);

        AccountId fromAccountId = Mockito.mock(AccountId.class);
        AccountId toAccountId = Mockito.mock(AccountId.class);

        TransactionId transactionId = Mockito.mock(TransactionId.class);
        Mockito.when(transactionId.toString()).thenReturn(TRANSACTION_ID);

        Transaction transaction = Mockito.mock(Transaction.class);
        Mockito.when(transaction.getAggregateId()).thenReturn(transactionId);
        Mockito.when(transaction.getAmount()).thenReturn(100);
        Mockito.when(transaction.getFromAccount()).thenReturn(fromAccountId);
        Mockito.when(transaction.getToAccount()).thenReturn(toAccountId);
        Mockito.when(transaction.getStatus()).thenReturn(TransactionStatus.PENDING);

        TransactionService transactionService = Mockito.mock(TransactionService.class);
        Mockito.when(transactionService.createTransaction(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(transactionId);
        Mockito.doThrow(new AggregateNotFoundException()).when(transactionService).execute(transactionId);

        TransactionController transactionController = new TransactionController(transactionService);
        Assertions.assertThrows(NotFoundResponse.class, () -> {
            transactionController.create(context);
        });
    }

    @Test
    void test_createTransactionFailureException() throws AggregateNotFoundException, TransactionFailureException {
        Transact transact = Mockito.mock(Transact.class);
        Validator bodyValidator = Mockito.mock(Validator.class);
        Mockito.when(bodyValidator.get()).thenReturn(transact);

        Validator paramValidator = Mockito.mock(Validator.class);
        Mockito.when(paramValidator.get()).thenReturn(TRANSACTION_ID);
        Context context = Mockito.mock(Context.class);
        Mockito.when(context.pathParam(Mockito.anyString(), Mockito.any())).thenReturn(paramValidator);
        Mockito.when(context.bodyValidator(Mockito.any())).thenReturn(bodyValidator);

        AccountId fromAccountId = Mockito.mock(AccountId.class);
        AccountId toAccountId = Mockito.mock(AccountId.class);

        TransactionId transactionId = Mockito.mock(TransactionId.class);
        Mockito.when(transactionId.toString()).thenReturn(TRANSACTION_ID);

        Transaction transaction = Mockito.mock(Transaction.class);
        Mockito.when(transaction.getAggregateId()).thenReturn(transactionId);
        Mockito.when(transaction.getAmount()).thenReturn(100);
        Mockito.when(transaction.getFromAccount()).thenReturn(fromAccountId);
        Mockito.when(transaction.getToAccount()).thenReturn(toAccountId);
        Mockito.when(transaction.getStatus()).thenReturn(TransactionStatus.PENDING);

        TransactionService transactionService = Mockito.mock(TransactionService.class);
        Mockito.when(transactionService.createTransaction(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(transactionId);
        Mockito.doThrow(new TransactionFailureException()).when(transactionService).execute(transactionId);

        TransactionController transactionController = new TransactionController(transactionService);
        transactionController.create(context);

        Mockito.verify(context).header(Header.LOCATION, transactionId.toString());
        Mockito.verify(context).status(Server.ACCEPTED);
    }
}