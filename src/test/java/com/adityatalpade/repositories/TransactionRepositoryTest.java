package com.adityatalpade.repositories;

import com.adityatalpade.domain.Transaction;
import com.adityatalpade.domain.TransactionId;
import com.adityatalpade.es.*;
import com.adityatalpade.exception.AggregateNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

class TransactionRepositoryTest {
    @Test
    void test_store() {
        EventStore eventStore = Mockito.mock(EventStore.class);
        AggregateId aggregateId = Mockito.mock(TransactionId.class);
        List<Event> changes = Mockito.mock(List.class);
        Aggregate aggregate = Mockito.mock(Transaction.class);
        Mockito.when(aggregate.getAggregateId()).thenReturn(aggregateId);
        Mockito.when(aggregate.getChanges()).thenReturn(changes);
        Repository repository = new TransactionRepository(eventStore);
        repository.store(aggregate);
        Mockito.verify(eventStore).store(aggregateId, changes);
    }

    @Test
    void test_loadEmpty() {
        EventStore eventStore = Mockito.mock(EventStore.class);
        TransactionId aggregateId = Mockito.mock(TransactionId.class);
        Repository repository = new TransactionRepository(eventStore);
        Assertions.assertThrows(AggregateNotFoundException.class, () -> {
            repository.load(aggregateId);
        });
    }

    @Test
    void test_load() throws AggregateNotFoundException {
        Aggregate aggregate = Mockito.mock(Transaction.class);

        List<Event> events = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Event event = Mockito.mock(Event.class);
            events.add(event);
        }
        List<Event> eventsSpy = Mockito.spy(events);

        AggregateId aggregateId = Mockito.mock(TransactionId.class);

        EventStream eventStream = Mockito.mock(EventStream.class);
        Mockito.when(eventStream.getStream()).thenReturn(eventsSpy);

        EventStore eventStore = Mockito.mock(EventStore.class);
        Mockito.when(eventStore.loadEventStream(aggregateId)).thenReturn(eventStream);

        Mockito.when(aggregate.getAggregateId()).thenReturn(aggregateId);
        Mockito.when(aggregate.getChanges()).thenReturn(eventsSpy);
        Repository repository = new TransactionRepository(eventStore);
        repository.store(aggregate);

        Aggregate loadedAggregate = repository.load(aggregateId);

        Assertions.assertTrue(loadedAggregate instanceof Transaction);
        for (Event event : eventsSpy) {
            Mockito.verify(event, Mockito.times(1)).applyOn(Mockito.any(Aggregate.class));
        }
    }
}