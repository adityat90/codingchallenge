package com.adityatalpade.es;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

class InMemoryEventStreamTest {
    @Test
    void test_emptyEventStream() {
        EventStream empty = InMemoryEventStream.empty();
        Assertions.assertEquals(0, empty.getStream().size());
    }

    @Test
    void test_AddAll() {
        List<Event> events = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            events.add(Mockito.mock(Event.class));
        }
        EventStream empty = InMemoryEventStream.empty();
        empty.addAll(events);

        Assertions.assertEquals(10, empty.getStream().size());
        for (int i = 0; i < events.size(); i++) {
            Assertions.assertEquals(events.get(i), empty.getStream().get(i));
        }
    }


}