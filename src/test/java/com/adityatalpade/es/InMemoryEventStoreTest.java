package com.adityatalpade.es;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

class InMemoryEventStoreTest {
    @Test
    void test_emptyStore() {
        AggregateId aggregateId = Mockito.mock(AggregateId.class);
        EventStore eventStore = new InMemoryEventStore();
        EventStream eventStream = eventStore.loadEventStream(aggregateId);
        Assertions.assertNull(eventStream);
    }

    @Test
    void test_storeStream() {
        List<Event> events = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            events.add(Mockito.mock(Event.class));
        }
        AggregateId aggregateId = Mockito.mock(AggregateId.class);
        EventStore eventStore = new InMemoryEventStore();

        eventStore.store(aggregateId, events);

        EventStream eventStream = eventStore.loadEventStream(aggregateId);

        Assertions.assertEquals(10, eventStream.getStream().size());
        for (int i = 0; i < events.size(); i++) {
            Assertions.assertEquals(events.get(i), eventStream.getStream().get(i));
        }
    }
}