package com.adityatalpade.domain;

import com.adityatalpade.events.AccountCreatedEvent;
import com.adityatalpade.events.AccountCreditedEvent;
import com.adityatalpade.events.AccountDebitedEvent;
import com.adityatalpade.exception.InsufficientFundsException;
import com.adityatalpade.exception.InvalidAmountException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class AccountTest {
    @Test
    void test_create() {
        Account account = Account.create();
        Assertions.assertNotNull(account.getAggregateId());
        Assertions.assertNotNull(account.getChanges());
        Assertions.assertEquals(1, account.getChanges().size());
        Assertions.assertTrue(account.getChanges().get(0) instanceof AccountCreatedEvent);
        Assertions.assertEquals(0, account.getBalance());
    }

    @Test
    void test_existingAccount() {
        AccountId accountId = Mockito.mock(AccountId.class);
        Account account = Account.from(accountId);
        Assertions.assertNotNull(account.getAggregateId());
        Assertions.assertEquals(accountId, account.getAggregateId());
        Assertions.assertNotNull(account.getChanges());
        Assertions.assertEquals(0, account.getChanges().size());
        Assertions.assertNull(account.getBalance());
    }

    @Test
    void test_applyAccountCreatedEvent() {
        AccountCreatedEvent event = Mockito.mock(AccountCreatedEvent.class);
        Account account = Account.create();
        account.apply(event);
        Assertions.assertEquals(0, account.getBalance());
    }

    @Test
    void test_applyAccountCreditedEvent() {
        AccountCreditedEvent event = Mockito.mock(AccountCreditedEvent.class);
        Mockito.when(event.getAmount()).thenReturn(100);
        Account account = Account.create();
        account.apply(event);
        Assertions.assertEquals(100, account.getBalance());
    }

    @Test
    void test_applyAccountDebitedEvent() {
        AccountCreditedEvent creditedEvent = Mockito.mock(AccountCreditedEvent.class);
        Mockito.when(creditedEvent.getAmount()).thenReturn(100);
        AccountDebitedEvent debitedEvent = Mockito.mock(AccountDebitedEvent.class);
        Mockito.when(debitedEvent.getAmount()).thenReturn(100);
        Account account = Account.create();
        account.apply(creditedEvent);
        account.apply(debitedEvent);
        Assertions.assertEquals(0, account.getBalance());
    }

    @Test
    void test_credit() throws InvalidAmountException {
        Account account = Account.create();
        account.credit(100);
        Assertions.assertEquals(100, account.getBalance());
    }

    @Test
    void test_creditInvalidAmount() {
        Account account = Account.create();
        Assertions.assertThrows(InvalidAmountException.class, () -> {
            account.credit(-100);
        });
        Assertions.assertEquals(0, account.getBalance());
    }

    @Test
    void test_debit() throws InvalidAmountException, InsufficientFundsException {
        Account account = Account.create();
        account.credit(200);
        account.debit(100);
        Assertions.assertEquals(100, account.getBalance());
    }

    @Test
    void test_debitInvalidAmount() {
        Account account = Account.create();
        Assertions.assertThrows(InvalidAmountException.class, () -> {
            account.debit(-100);
        });
        Assertions.assertEquals(0, account.getBalance());
    }

    @Test
    void test_debitInsufficientFunds() {
        Account account = Account.create();
        Assertions.assertThrows(InsufficientFundsException.class, () -> {
            account.debit(100);
        });
        Assertions.assertEquals(0, account.getBalance());
    }
}