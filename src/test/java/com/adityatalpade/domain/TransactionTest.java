package com.adityatalpade.domain;

import com.adityatalpade.events.TransactionCreatedEvent;
import com.adityatalpade.events.TransactionFailedEvent;
import com.adityatalpade.events.TransactionSucceededEvent;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class TransactionTest {
    @Test
    void test_createTransaction() {
        AccountId fromAccountId = Mockito.mock(AccountId.class);
        AccountId toAccountId = Mockito.mock(AccountId.class);
        Transaction transaction = Transaction.create(fromAccountId, toAccountId, 100);
        Assertions.assertEquals(fromAccountId, transaction.getFromAccount());
        Assertions.assertEquals(toAccountId, transaction.getToAccount());
        Assertions.assertEquals(100, transaction.getAmount());
        Assertions.assertEquals(TransactionStatus.PENDING, transaction.getStatus());
        Assertions.assertEquals(1, transaction.getChanges().size());
        Assertions.assertTrue(transaction.getChanges().get(0) instanceof TransactionCreatedEvent);
    }

    @Test
    void test_disburse() {
        AccountId fromAccountId = Mockito.mock(AccountId.class);
        AccountId toAccountId = Mockito.mock(AccountId.class);
        Transaction transaction = Transaction.create(fromAccountId, toAccountId, 100);
        transaction.disburse();
        Assertions.assertEquals(fromAccountId, transaction.getFromAccount());
        Assertions.assertEquals(toAccountId, transaction.getToAccount());
        Assertions.assertEquals(100, transaction.getAmount());
        Assertions.assertEquals(TransactionStatus.SUCCESS, transaction.getStatus());
        Assertions.assertEquals(2, transaction.getChanges().size());
        Assertions.assertTrue(transaction.getChanges().get(1) instanceof TransactionSucceededEvent);
    }

    @Test
    void test_stop() {
        AccountId fromAccountId = Mockito.mock(AccountId.class);
        AccountId toAccountId = Mockito.mock(AccountId.class);
        Transaction transaction = Transaction.create(fromAccountId, toAccountId, 100);
        transaction.stop();
        Assertions.assertEquals(fromAccountId, transaction.getFromAccount());
        Assertions.assertEquals(toAccountId, transaction.getToAccount());
        Assertions.assertEquals(100, transaction.getAmount());
        Assertions.assertEquals(TransactionStatus.FAILED, transaction.getStatus());
        Assertions.assertEquals(2, transaction.getChanges().size());
        Assertions.assertTrue(transaction.getChanges().get(1) instanceof TransactionFailedEvent);
    }

    @Test
    void test_applyTransactionCreatedEvent() {
        TransactionId transactionId = Mockito.mock(TransactionId.class);
        AccountId fromAccountId = Mockito.mock(AccountId.class);
        AccountId toAccountId = Mockito.mock(AccountId.class);
        TransactionCreatedEvent event = Mockito.mock(TransactionCreatedEvent.class);
        Mockito.when(event.getAmount()).thenReturn(100);
        Mockito.when(event.getAggregateId()).thenReturn(transactionId);
        Mockito.when(event.getFromAccount()).thenReturn(fromAccountId);
        Mockito.when(event.getToAccount()).thenReturn(toAccountId);

        Transaction transaction = Transaction.from(transactionId);

        transaction.apply(event);

        Assertions.assertEquals(100, transaction.getAmount());
        Assertions.assertEquals(fromAccountId, transaction.getFromAccount());
        Assertions.assertEquals(toAccountId, transaction.getToAccount());
        Assertions.assertEquals(TransactionStatus.PENDING, transaction.getStatus());
    }

    @Test
    void test_applyTransactionSucceededEvent() {
        TransactionId transactionId = Mockito.mock(TransactionId.class);
        AccountId fromAccountId = Mockito.mock(AccountId.class);
        AccountId toAccountId = Mockito.mock(AccountId.class);
        TransactionSucceededEvent event = Mockito.mock(TransactionSucceededEvent.class);

        Transaction transaction = Transaction.from(transactionId);

        transaction.apply(event);

        Assertions.assertEquals(TransactionStatus.SUCCESS, transaction.getStatus());
    }

    @Test
    void test_applyTransactionFailedEvent() {
        TransactionId transactionId = Mockito.mock(TransactionId.class);
        AccountId fromAccountId = Mockito.mock(AccountId.class);
        AccountId toAccountId = Mockito.mock(AccountId.class);
        TransactionFailedEvent event = Mockito.mock(TransactionFailedEvent.class);

        Transaction transaction = Transaction.from(transactionId);

        transaction.apply(event);

        Assertions.assertEquals(TransactionStatus.FAILED, transaction.getStatus());
    }
}