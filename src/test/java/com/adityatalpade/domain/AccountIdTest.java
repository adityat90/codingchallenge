package com.adityatalpade.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class AccountIdTest {

    public static final String ACCOUNT_ID = "ACCOUNT_ID";

    @Test
    public void test_create() {
        AccountId accountId = AccountId.generate();
        Assertions.assertNotNull(accountId);
    }

    @Test
    public void test_from() {
        AccountId accountId = AccountId.from(ACCOUNT_ID);
        Assertions.assertEquals(ACCOUNT_ID, accountId.toString());
    }

    @Test
    public void test_equals() {
        AccountId accountId1 = AccountId.from(ACCOUNT_ID);
        AccountId accountId2 = AccountId.from(ACCOUNT_ID);
        Assertions.assertEquals(accountId1, accountId2);
    }
}