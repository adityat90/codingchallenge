package com.adityatalpade.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TransactionIdTest {

    public static final String TRANSACTION_ID = "TRANSACTION_ID";

    @Test
    public void test_create() {
        TransactionId transactionId = TransactionId.generate();
        Assertions.assertNotNull(transactionId);
    }

    @Test
    public void test_from() {
        TransactionId transactionId = TransactionId.from(TRANSACTION_ID);
        Assertions.assertEquals(TRANSACTION_ID, transactionId.toString());
    }

    @Test
    public void test_equals() {
        TransactionId transactionId1 = TransactionId.from(TRANSACTION_ID);
        TransactionId transactionId2 = TransactionId.from(TRANSACTION_ID);
        Assertions.assertEquals(transactionId1, transactionId2);
    }
}