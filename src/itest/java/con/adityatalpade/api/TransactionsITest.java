package con.adityatalpade.api;

import com.adityatalpade.api.Server;
import com.adityatalpade.api.model.AccountProjection;
import com.adityatalpade.api.model.Credit;
import com.adityatalpade.api.model.Transact;
import com.adityatalpade.api.model.TransactionProjection;
import com.adityatalpade.di.Module;
import com.google.inject.Guice;
import com.google.inject.Injector;
import io.javalin.core.util.Header;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TransactionsITest {
    private static final String ACCOUNTS = "accounts/";
    private static final String CREDIT = "/credit";
    private static final String URI = "http://localhost:4000/";
    private static final Injector injector = Guice.createInjector(new Module());
    private static final Server server = injector.getInstance(Server.class);
    private static final String UNKNOWN_ID = "UNKNOWN_ID";
    private static final String TRANSACTIONS = "transactions/";

    @BeforeAll
    static void setup() {
        server.start(4000);
    }

    HttpResponse createAccount() {
        return Unirest.post(URI + ACCOUNTS)
                .header("Accept", "application/json")
                .asEmpty();
    }

    HttpResponse creditAccount(String accountId, Integer amount) {
        return Unirest.post(URI + ACCOUNTS + accountId + CREDIT)
                .header("Accept", "application/json")
                .body(new Credit(amount))
                .asEmpty();
    }

    HttpResponse getAccount(String accountId) {
        return Unirest.get(URI + ACCOUNTS + accountId)
                .header("Accept", "application/json")
                .asObject(AccountProjection.class);
    }

    HttpResponse createTransaction(String fromAccountId, String toAccountId, Integer amount) {
        return Unirest.post(URI + TRANSACTIONS)
                .header("Accept", "application/json")
                .body(new Transact(fromAccountId, toAccountId, amount))
                .asEmpty();
    }

    HttpResponse getTransaction(String transactionId) {
        return Unirest.get(URI + TRANSACTIONS + transactionId)
                .header("Accept", "application/json")
                .asObject(TransactionProjection.class);
    }

    @Test
    void test_createTransaction() {
        HttpResponse createAccount1Response = createAccount();
        String fromAccount = createAccount1Response.getHeaders().getFirst(Header.LOCATION);

        HttpResponse createAccount2Response = createAccount();
        String toAccount = createAccount2Response.getHeaders().getFirst(Header.LOCATION);

        creditAccount(fromAccount, 200);

        HttpResponse createTransaction1Response = createTransaction(fromAccount, toAccount, 200);
        Assertions.assertEquals(202, createTransaction1Response.getStatus());

        String transaction1Id = createTransaction1Response.getHeaders().getFirst(Header.LOCATION);
        Assertions.assertNotNull(transaction1Id);

        HttpResponse getTransaction1Response = getTransaction(transaction1Id);
        Assertions.assertEquals(200, getTransaction1Response.getStatus());
        Assertions.assertTrue(getTransaction1Response.getBody() instanceof TransactionProjection);
        Assertions.assertEquals(fromAccount, ((TransactionProjection) getTransaction1Response.getBody()).getFromAccountId());
        Assertions.assertEquals(toAccount, ((TransactionProjection) getTransaction1Response.getBody()).getToAccountId());
        Assertions.assertEquals("SUCCESS", ((TransactionProjection) getTransaction1Response.getBody()).getTransactionStatus());
    }

    @Test
    void test_createParallelTransaction() {
        HttpResponse createAccount1Response = createAccount();
        String fromAccount = createAccount1Response.getHeaders().getFirst(Header.LOCATION);

        HttpResponse createAccount2Response = createAccount();
        String toAccount = createAccount2Response.getHeaders().getFirst(Header.LOCATION);

        creditAccount(fromAccount, 200);

        List<String> transactionIds = IntStream.range(0, 1000)
                .parallel()
                .mapToObj(value -> {
                    HttpResponse createTransaction1Response = createTransaction(fromAccount, toAccount, 100);
                    Assertions.assertEquals(202, createTransaction1Response.getStatus());
                    return createTransaction1Response.getHeaders().getFirst(Header.LOCATION);
                }).collect(Collectors.toList());

        List<String> statuses = transactionIds
                .stream()
                .map(s -> {
                    HttpResponse transaction = getTransaction(s);
                    return ((TransactionProjection) transaction.getBody()).getTransactionStatus();
                }).filter("SUCCESS"::equals).collect(Collectors.toList());

        Assertions.assertEquals(2, statuses.size());

        HttpResponse fromAccountResponse = getAccount(fromAccount);
        Assertions.assertEquals(200, fromAccountResponse.getStatus());
        Assertions.assertTrue(fromAccountResponse.getBody() instanceof AccountProjection);
        Assertions.assertEquals(fromAccount, ((AccountProjection) fromAccountResponse.getBody()).getAccountId());
        Assertions.assertEquals(0, ((AccountProjection) fromAccountResponse.getBody()).getBalance());

        HttpResponse toAccountResponse = getAccount(toAccount);
        Assertions.assertEquals(200, toAccountResponse.getStatus());
        Assertions.assertTrue(toAccountResponse.getBody() instanceof AccountProjection);
        Assertions.assertEquals(toAccount, ((AccountProjection) toAccountResponse.getBody()).getAccountId());
        Assertions.assertEquals(200, ((AccountProjection) toAccountResponse.getBody()).getBalance());

    }
}
