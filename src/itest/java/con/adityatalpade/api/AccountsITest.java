package con.adityatalpade.api;

import com.adityatalpade.api.Server;
import com.adityatalpade.api.model.AccountProjection;
import com.adityatalpade.api.model.Credit;
import com.adityatalpade.api.model.Debit;
import com.adityatalpade.di.Module;
import com.google.inject.Guice;
import com.google.inject.Injector;
import io.javalin.core.util.Header;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

public class AccountsITest {
    private static final String ACCOUNTS = "accounts/";
    private static final String CREDIT = "/credit";
    private static final String DEBIT = "/debit";
    private static final String URI = "http://localhost:3000/";
    private static final Injector injector = Guice.createInjector(new Module());
    private static final Server server = injector.getInstance(Server.class);
    private static final String UNKNOWN_ID = "UNKNOWN_ID";

    @BeforeAll
    static void setup() {
        server.start(3000);
    }

    HttpResponse createAccount() {
        return Unirest.post(URI + ACCOUNTS)
                .header("Accept", "application/json")
                .asEmpty();
    }

    HttpResponse creditAccount(String accountId, Integer amount) {
        return Unirest.post(URI + ACCOUNTS + accountId + CREDIT)
                .header("Accept", "application/json")
                .body(new Credit(amount))
                .asEmpty();
    }

    HttpResponse debitAccount(String accountId, Integer amount) {
        return Unirest.post(URI + ACCOUNTS + accountId + DEBIT)
                .header("Accept", "application/json")
                .body(new Debit(amount))
                .asEmpty();
    }

    HttpResponse getAccount(String accountId) {
        return Unirest.get(URI + ACCOUNTS + accountId)
                .header("Accept", "application/json")
                .asObject(AccountProjection.class);
    }

    @Test
    void test_accountCreate() {
        HttpResponse httpResponse = createAccount();
        String location = httpResponse.getHeaders().getFirst(Header.LOCATION);
        Assertions.assertNotEquals("", location);
        Assertions.assertEquals(201, httpResponse.getStatus());
    }

    @Test
    void test_accountOperations() {
        HttpResponse createAccountResponse = createAccount();
        String accountId = createAccountResponse.getHeaders().getFirst(Header.LOCATION);

        HttpResponse creditAccountResponse1 = creditAccount(accountId, 200);
        Assertions.assertEquals(202, creditAccountResponse1.getStatus());

        HttpResponse creditAccountResponse2 = creditAccount(accountId, -200);
        Assertions.assertEquals(400, creditAccountResponse2.getStatus());

        HttpResponse getAccountResponse1 = getAccount(accountId);
        Assertions.assertEquals(200, getAccountResponse1.getStatus());
        Assertions.assertTrue(getAccountResponse1.getBody() instanceof AccountProjection);
        Assertions.assertEquals(accountId, ((AccountProjection) getAccountResponse1.getBody()).getAccountId());
        Assertions.assertEquals(200, ((AccountProjection) getAccountResponse1.getBody()).getBalance());

        HttpResponse debitAccountResponse = debitAccount(accountId, 200);
        Assertions.assertEquals(202, debitAccountResponse.getStatus());

        HttpResponse getAccountResponse2 = getAccount(accountId);
        Assertions.assertEquals(200, getAccountResponse2.getStatus());
        Assertions.assertTrue(getAccountResponse2.getBody() instanceof AccountProjection);
        Assertions.assertEquals(accountId, ((AccountProjection) getAccountResponse2.getBody()).getAccountId());
        Assertions.assertEquals(0, ((AccountProjection) getAccountResponse2.getBody()).getBalance());

        HttpResponse debitAccountResponse1 = debitAccount(accountId, -200);
        Assertions.assertEquals(400, debitAccountResponse1.getStatus());

        HttpResponse debitAccountResponse2 = debitAccount(accountId, 300);
        Assertions.assertEquals(400, debitAccountResponse2.getStatus());

        HttpResponse getAccountResponse3 = getAccount(UNKNOWN_ID);
        Assertions.assertEquals(404, getAccountResponse3.getStatus());

        HttpResponse creditAccountResponse3 = creditAccount(UNKNOWN_ID, 200);
        Assertions.assertEquals(404, creditAccountResponse3.getStatus());

        HttpResponse debitAccountResponse3 = debitAccount(UNKNOWN_ID, 200);
        Assertions.assertEquals(404, debitAccountResponse3.getStatus());
    }

    @Test
    void test_parallelAccountOperations() {
        HttpResponse createAccountResponse = createAccount();
        String accountId = createAccountResponse.getHeaders().getFirst(Header.LOCATION);

        HttpResponse creditAccountResponse1 = creditAccount(accountId, 200);
        Assertions.assertEquals(202, creditAccountResponse1.getStatus());

        IntStream.range(1, 10).parallel().forEach(a -> {
            HttpResponse creditAccountResponse2 = creditAccount(accountId, 200);
            Assertions.assertEquals(202, creditAccountResponse2.getStatus());
            IntStream.range(1, 1000)
                    .parallel()
                    .forEach(b -> {
                        HttpResponse debitAccountResponse = debitAccount(accountId, 100);
                    });
        });
        HttpResponse getAccountResponse = getAccount(accountId);
        Assertions.assertEquals(200, getAccountResponse.getStatus());
        Assertions.assertTrue(getAccountResponse.getBody() instanceof AccountProjection);
        Assertions.assertEquals(accountId, ((AccountProjection) getAccountResponse.getBody()).getAccountId());
        Assertions.assertEquals(0, ((AccountProjection) getAccountResponse.getBody()).getBalance());
    }
}
